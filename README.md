# The Sims Online Restoration (TSOR)

TSOR was the first attempt at recreating both The Sims Online's client and server.

The New and Restored splash was my own creation.

## Scam?

A common misconception that all of TSOR was a scam. This is not entirely true. Only the donations and TSOEmu were scams but TSOR itself was not. Both Project Dollhouse and FreeSO are both direct descendants of that code.

Afr0, one of the developers of TSOR, posted that code on Google Code. I snatched a copy of it and posting it here.