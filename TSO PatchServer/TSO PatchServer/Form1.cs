﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Net;
using TSO_PatchServer.Patch;
using TSO_PatchServer.Network;
using FTPLibD;

namespace TSO_PatchServer
{
    public partial class Form1 : Form
    {
        private Patcher m_Patcher;

        private FTPListener m_FTPListener;
        private PatchListener m_Listener;
        //The client used to connect to the login server.
        private LoginClient m_LoginClient;

        public Form1()
        {
            InitializeComponent();

            Initialize();
        }

        /// <summary>
        /// Performs initialization neccessary to start the patch server.
        /// </summary>
        private void Initialize()
        {
            Logger.Initialize(Environment.CurrentDirectory + "\\Log.txt");
            Logger.WarnEnabled = true;
            Logger.DebugEnabled = true;
            Logger.InfoEnabled = true;

            try
            {
                m_Patcher = new Patcher();
            }
            catch (NoVersionException NoVersion)
            {
                if (Logger.WarnEnabled)
                    Logger.LogWarning("Couldn't find 'Version.txt'! Defaulting to version 1.0.0.0!\r\n" +
                        NoVersion.ToString() + "\r\n\r\n");
            }

            LblCurrentVersion.Text = "Current Version: " + GlobalSettings.Default.PatchVersion;

            m_LoginClient = new LoginClient(GlobalSettings.Default.LoginIP, GlobalSettings.Default.LoginPort, 
                "Login Server");
            m_LoginClient.OnReceiveEvent += new OnLoginReceiveDelegate(m_LoginClient_OnReceiveEvent);
            //Login packet from the LoginServer.
            LoginClient.RegisterLoginPacketID(0x81, 17);

            WaitHandle.WaitAny(new AutoResetEvent[] {m_LoginClient.ConnectingWaitEvent});

            PatchClient.RegisterPatchPacketID(0x90, 13);

            m_FTPListener = new FTPListener();
            m_FTPListener.Initialize(26300, "10.0.0.1");
            m_FTPListener.OnReceiveEvent += new OnFTPReceiveDelegate(m_FTPListener_OnReceiveEvent);

            Database.Connect();

            m_Listener = new PatchListener();
            m_Listener.OnReceiveEvent += new OnReceiveDelegate(m_Listener_OnReceiveEvent);
            m_Listener.Initialize(GlobalSettings.Default.ClientPort);

            SendLoginPacket(m_LoginClient);

            return;
        }

        private void m_FTPListener_OnReceiveEvent(string FTPCommand, FTPClient Client)
        {
            string Delimiter = " ";
            string Cmd = FTPCommand.Split(Delimiter.ToCharArray())[0].Replace("\r\n", "");

            switch (Cmd)
            {
                case "USER":
                    PacketHandlers.HandleUSERCmd(FTPCommand, Client);
                    break;
                case "PASS":
                    PacketHandlers.HandlePASSCmd(FTPCommand, Client);
                    break;
                case "SYST":
                    PacketHandlers.HandleSYSTCmd(FTPCommand, Client);
                    break;
                case "FEAT":
                    PacketHandlers.HandleFEATCmd(FTPCommand, Client);
                    break;
                case "OPTS":
                    PacketHandlers.HandleOPTSCmd(FTPCommand, Client);
                    break;
                case "PWD":
                    PacketHandlers.HandlePWDCmd(FTPCommand, Client);
                    break;
                case "TYPE":
                    PacketHandlers.HandleTYPECmd(FTPCommand, Client);
                    break;
                case "PASV":
                    PacketHandlers.HandlePASVCmd(FTPCommand, /*m_FTPListener.IPAddress*/"88.89.137.148", Client);
                    break;
                case "LIST":
                    PacketHandlers.HandleLISTCmd(FTPCommand, Client);
                    break;
                case "CLNT": //Undocumented command from http://www.ftptest.net...
                    Client.Send(Encoding.ASCII.GetBytes("200 Don't care\r\n"));
                    break;
                case "MLSD":
                    PacketHandlers.HandleMLSDCmd(FTPCommand, Client);
                    break;
                default:
                    MessageBox.Show("'" + FTPCommand + "'");
                    break;
            }
        }

        private void m_LoginClient_OnReceiveEvent(PacketStream P, LoginClient Client)
        {
            byte ID = (byte)P.ReadByte();

            switch (ID)
            {
                //Login packet - means a client connected to the Login Server and wants to transfer!
                case 0x81:
                    PacketHandlers.HandleLoginPacket(P, Client);
                    break;
                default:
                    break;
            }
        }

        void m_Listener_OnReceiveEvent(PacketStream P, PatchClient Client)
        {
            byte ID = (byte)P.ReadByte();

            switch (ID)
            {
                case 0x90:
                    PacketHandlers.HandleClientLogin(P, Client);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Sends a login packet to the login server, containing
        /// this server's ClientPort and IP.
        /// </summary>
        private void SendLoginPacket(LoginClient Client)
        {
            PacketStream P = new PacketStream(0x84, 13);
            P.WriteByte(0x84);
            P.WriteInt32(GlobalSettings.Default.ClientPort);
            P.WriteInt64(NetworkHelp.IP2Long(m_Listener.IPAddress));

            Client.Send(P.ToArray());
        }
    }
}
