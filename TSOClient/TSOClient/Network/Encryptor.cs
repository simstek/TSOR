﻿using System;
using System.Collections.Generic;
using System.Text;
using TSOClient.Network.Encryption;

namespace TSOClient.Network
{
    /// <summary>
    /// Class that holds the neccessary components for encrypting the client's network connection.
    /// </summary>
    public class Encryptor
    {
        private BigInteger m_Credentials;
        private SecureRemotePassword m_SRP;

        public SecureRemotePassword SRP
        {
            get { return m_SRP; }
            set { m_SRP = value; }
        }

        public Encryptor(string Username, string Password)
        {
            m_Credentials = HashUtilities.HashToBigInteger(SecureRemotePassword.SRPParameters.Hash, 
                Username + ":" + Password);
            m_SRP = new SecureRemotePassword(Username, m_Credentials, false, 
                SecureRemotePassword.SRPParameters.Defaults);
        }
    }
}
