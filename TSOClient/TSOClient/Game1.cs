using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;
using TSOClient.LUI;
using TSOClient.Network;
using SimsLib.FAR3;
using LogThis;
using NAudio.Wave;
using Un4seen.Bass;

namespace TSOClient
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        public ScreenManager ScreenMgr;

        private Dictionary<int, string> m_TextDict = new Dictionary<int, string>();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            Log.UseSensibleDefaults();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;

            BassNet.Registration("afr088@hotmail.com", "2X3163018312422");
            Bass.BASS_Init(-1, 8000, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero, System.Guid.Empty);

            this.IsMouseVisible = true;

            //Might want to reconsider this...
            this.IsFixedTimeStep = false;
            graphics.SynchronizeWithVerticalRetrace = false;

            //InitLoginNotify - 65 bytes
            NetworkClient.RegisterLoginPacketID(0x01, 65);
            //LoginFailResponse - 2 bytes
            NetworkClient.RegisterLoginPacketID(0x02, 2);
            //LoginSuccessResponse - 33 bytes
            NetworkClient.RegisterLoginPacketID(0x04, 33);

            //This should ideally be stored in the Windows Registry...
            GlobalSettings.Default.StartupPath = "C:\\Program Files\\Maxis\\The Sims Online\\TSOClient\\";

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            int Channel = Bass.BASS_StreamCreateFile("Sounds\\BUTTON.WAV", 0, 0, BASSFlag.BASS_DEFAULT);
            UISounds.AddSound(new UISound(0x01, Channel));

            ScreenMgr = new ScreenManager(this, Content.Load<SpriteFont>("ComicSans"),
                Content.Load<SpriteFont>("ComicSansSmall"));

            //Make the screenmanager and the startup path globally available to all Lua scripts.
            LuaInterfaceManager.ExportObject("ScreenManager", ScreenMgr);
            LuaInterfaceManager.ExportObject("StartupPath", GlobalSettings.Default.StartupPath);
            
            //Read settings...
            LuaFunctions.ReadSettings("gamedata\\settings\\settings.lua");

            LoadStrings();
            ScreenMgr.TextDict = m_TextDict;

            ScreenMgr.LoadInitialScreen("gamedata\\luascripts\\login.lua");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        private float m_FPS = 0;

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            m_FPS = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            // TODO: Add your update logic here
            ScreenMgr.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin(SpriteBlendMode.AlphaBlend);

            ScreenMgr.Draw(spriteBatch, m_FPS);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Loads the correct set of strings based on the current language.
        /// This method is a bit of a hack, but it works.
        /// </summary>
        private void LoadStrings()
        {
            string CurrentLang = GlobalSettings.Default.CurrentLang.ToLower();

            LuaInterfaceManager.RunFileInThread("gamedata\\uitext\\luatext\\" +
                CurrentLang + "\\" + CurrentLang + ".lua");

            m_TextDict.Add(1, (string)LuaInterfaceManager.LuaVM["LoginName"]);
            m_TextDict.Add(2, (string)LuaInterfaceManager.LuaVM["LoginPass"]);
            m_TextDict.Add(3, (string)LuaInterfaceManager.LuaVM["Login"]);
            m_TextDict.Add(4, (string)LuaInterfaceManager.LuaVM["Exit"]);
            m_TextDict.Add(5, (string)LuaInterfaceManager.LuaVM["OverallProgress"]);
            m_TextDict.Add(6, (string)LuaInterfaceManager.LuaVM["CurrentTask"]);
            m_TextDict.Add(7, (string)LuaInterfaceManager.LuaVM["InfoPopup1"]);
            m_TextDict.Add(8, (string)LuaInterfaceManager.LuaVM["PersonSelectionCaption"]);
            m_TextDict.Add(9, (string)LuaInterfaceManager.LuaVM["TimeStart"]);
            m_TextDict.Add(10, (string)LuaInterfaceManager.LuaVM["PersonSelectionEditCaption"]);
        }
    }
}
