﻿using System;
using System.Collections.Generic;
using System.Text;
using NAudio.Wave;

namespace TSOClient.LUI
{
    public class UISound
    {
        private int m_ID = 0x00;
        private int m_Channel;

        public UISound(int ID, int Sound)
        {
            m_ID = ID;
            m_Channel = Sound;
        }

        public int ID
        {
            get { return m_ID; }
        }

        public int ThisChannel
        {
            get { return m_Channel; }
        }
    }

    /// <summary>
    /// Static class that contains preloaded UI-sounds.
    /// So far there's only one sound, the sound of a buttonclick.
    /// </summary>
    class UISounds
    {
        private static List<UISound> m_Sounds = new List<UISound>();

        public static void AddSound(UISound Sound)
        {
            m_Sounds.Add(Sound);
        }

        public static UISound GetSound(int ID)
        {
            foreach (UISound Sound in m_Sounds)
            {
                if(ID == Sound.ID)
                    return Sound;
            }

            return null;
        }
    }
}
