﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TSOClient.LUI
{
    /// <summary>
    /// A drawable image that is part of the GUI.
    /// Cannot be clicked.
    /// </summary>
    public class UIImage : UIElement
    {
        private int m_X, m_Y;
        private Texture2D m_Texture;
        private string m_StrID;

        public int X
        {
            get { return m_X; }
        }

        public int Y
        {
            get { return m_Y; }
        }

        public Texture2D Texture
        {
            get { return m_Texture; }
        }

        public UIImage(int X, int Y, string StrID, Texture2D Texture, UIScreen Screen)
            : base(Screen, StrID, DrawLevel.DontGiveAFuck)
        {
            m_X = X;
            m_Y = Y;
            m_Texture = Texture;
            m_StrID = StrID;
        }
    }
}
