﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TSOClient.LUI
{
    /// <summary>
    /// Functions called from Lua.
    /// </summary>
    public class LuaFunctions
    {
        /// <summary>
        /// Called from Lua, in order to exit the application.
        /// </summary>
        /// <param name="ExitCode">The code to pass to the OS when exiting (usually 0).</param>
        public static void ApplicationExit(int ExitCode)
        {
            Environment.Exit(ExitCode);
        }

        /// <summary>
        /// Called at application startup, to process the application's settings from
        /// a Lua script.
        /// </summary>
        /// <param name="Path">The path to the Lua script containing the settings.</param>
        public static void ReadSettings(string Path)
        {
            LuaInterfaceManager.RunFileInThread(Path);
            
            GlobalSettings.Default.ShowHints = (bool)LuaInterfaceManager.LuaVM["ShowHints"];
            GlobalSettings.Default.CurrentLang = (string)LuaInterfaceManager.LuaVM["CurrentLang"];
            
            GlobalSettings.Default.LoginServerIP = (string)LuaInterfaceManager.LuaVM["LoginServerIP"];
            GlobalSettings.Default.LoginServerPort = (int)(double)LuaInterfaceManager.LuaVM["LoginServerPort"];
        }
    }
}
