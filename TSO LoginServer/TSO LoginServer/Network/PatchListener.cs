﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace TSO_LoginServer.Network
{
    public delegate void OnPatchReceiveDelegate(PacketStream P, PatchClient Client);

    public class PatchListener
    {
        private PatchClient m_PatchServer;
        private Socket m_ListenerSock;
        private IPEndPoint m_LocalEP;

        public PatchClient PatchServer
        {
            get { return m_PatchServer; }
        }

        public event OnPatchReceiveDelegate OnReceiveEvent;

        public PatchListener()
        {
            m_ListenerSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Initialize(int Port)
        {
            IPEndPoint LocalEP = new IPEndPoint(IPAddress.Any, Port);

            m_LocalEP = LocalEP;

            try
            {
                m_ListenerSock.Bind(LocalEP);
                m_ListenerSock.Listen(10000);

                Console.WriteLine("Started listening on: " + LocalEP.Address.ToString()
                    + ":" + LocalEP.Port);
            }
            catch (SocketException E)
            {
                Console.WriteLine("Winsock error caused by call to Socket.Bind(): \n" + E.ToString());
            }

            m_ListenerSock.BeginAccept(new AsyncCallback(OnAccept), m_ListenerSock);
        }

        public void OnAccept(IAsyncResult AR)
        {
            Socket AcceptedSocket = m_ListenerSock.EndAccept(AR);

            if (AcceptedSocket != null)
            {
                Console.WriteLine("\nNew client connected!");

                if (m_PatchServer == null)
                {
                    //Let sockets linger for 5 seconds after they're closed, in an attempt to make sure all
                    //pending data is sent!
                    AcceptedSocket.LingerState = new LingerOption(true, 5);
                    m_PatchServer = new PatchClient(AcceptedSocket, this);
                }
                else
                {
                    AcceptedSocket.Close();
                    AcceptedSocket.Disconnect(false);
                }
            }

            m_ListenerSock.BeginAccept(new AsyncCallback(OnAccept), m_ListenerSock);
        }

        /// <summary>
        /// Called by PatchClient instances
        /// when they've received some new data
        /// (a new packet). Should not be called
        /// from anywhere else.
        /// </summary>
        /// <param name="P"></param>
        public void OnReceivedData(PacketStream P, PatchClient Client)
        {
            OnReceiveEvent(P, Client);
        }
    }
}
